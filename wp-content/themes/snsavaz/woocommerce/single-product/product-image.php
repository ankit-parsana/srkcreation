<?php
/**
 * Single Product Image
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/product-image.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.5.1
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
wp_enqueue_script('owlcarousel');

global $post, $product;
$columns           = apply_filters( 'woocommerce_product_thumbnails_columns', 4 );
$post_thumbnail_id = get_post_thumbnail_id( $post->ID );
$full_size_image   = wp_get_attachment_image_src( $post_thumbnail_id, 'full' );
$image_title       = get_post_field( 'post_excerpt', $post_thumbnail_id );
$placeholder       = has_post_thumbnail() ? 'with-images' : 'without-images';
$wrapper_classes   = apply_filters( 'woocommerce_single_product_image_gallery_classes', array(
	'woocommerce-product-gallery',
	'woocommerce-product-gallery--' . $placeholder,
	'woocommerce-product-gallery--columns-' . absint( $columns ),
	'images',
) );
?>
<div class="images">
	<?php
	$attributes = array(
		'id'					  => 'sp_mainimage',
		'title'                   => $image_title,
		'data-src'                => $full_size_image[0],
		'data-large_image'        => $full_size_image[0],
		'data-large_image_width'  => $full_size_image[1],
		'data-large_image_height' => $full_size_image[2],
	);
	if ( has_post_thumbnail() ) {
		$attachment_count = count( $product->get_gallery_image_ids() );
		if ( $attachment_count > 0 ) {
			$gallery = '[product-gallery]';
		} else {
			$gallery = '';
		}
		$html = '';
		$html .= '<div data-thumb="' . get_the_post_thumbnail_url( $post->ID, 'shop_thumbnail' ) . '" class="woocommerce-product-gallery__image">';
		$html .= '<a class="woocommerce-main-image zoom" data-rel="prettyPhoto' . $gallery . '" href="' . esc_url( $full_size_image[0] ) . '">';
		$html .= get_the_post_thumbnail( $post->ID, 'shop_single', $attributes );
		$html .= '</a>';
		$html .= '</div>';
	} else {
		$html  = '<div class="woocommerce-product-gallery__image--placeholder">';
		$html .= sprintf( '<img src="%s" alt="%s" class="wp-post-image" />', esc_url( wc_placeholder_img_src() ), esc_html__( 'Awaiting product image', 'snsavaz' ) );
		$html .= '</div>';
	}
	echo apply_filters( 'woocommerce_single_product_image_thumbnail_html', $html, get_post_thumbnail_id( $post->ID ) );
	?>
	<div class="sns-thumbnails handle-preload">
		<?php do_action( 'woocommerce_product_thumbnails' ); ?>
		<div class="navslider">
			<span class="prev"><i class="fa fa-long-arrow-right"></i></span>
			<span class="next"><i class="fa fa-long-arrow-left"></i></span>
		</div>
	</div>
	<script type="text/javascript">
		jQuery(document).ready(function(){
			jQuery('.sns-thumbnails .thumbnails').owlCarousel({
				items: 4,
				responsive : {
				    0 : { items: 3 },
				    480 : { items: 4 },
				    768 : { items: 4 },
				    992 : { items: 4 },
				    1200 : { items: 4 }
				},
				loop:true,
	            dots: false,
	            // animateOut: 'flipInY',
			    //animateIn: 'pulse',
			    // autoplay: true,
	            onInitialized: callback,
	            slideSpeed : 800
			});
			function callback(event) {
	   			if(this._items.length > this.options.items){
			        jQuery('.sns-thumbnails .navslider').show();
			    }else{
			        jQuery('.sns-thumbnails .navslider').hide();
			    }
			}
			jQuery('.sns-thumbnails .prev').on('click', function(e){
				e.preventDefault();
				jQuery('.sns-thumbnails .thumbnails').trigger('prev.owl.carousel');
			});
			jQuery('.sns-thumbnails .next').on('click', function(e){
				e.preventDefault();
				jQuery('.sns-thumbnails .thumbnails').trigger('next.owl.carousel');
			});

			jQuery('.entry-img .images a.zoom').prettyPhoto({
				hook: 'data-rel',
				social_tools: false,
				theme: 'pp_woocommerce',
				horizontal_padding: 20,
				opacity: 0.8,
				deeplinking: false
			});
		});
	</script>

</div>
